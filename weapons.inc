/*

Romania Roleplay - Arme pe corp
2, 3, 5, 6, 7, 8, 9, 25, 29, 30, 31, 33, 34, 42
*/


/* PENTRU BAZA DE DATE

REATE TABLE IF NOT EXISTS `weapons` (
  `name` varchar(24) NOT NULL,
  `weaponID` tinyint(4) NOT NULL,
  `PosX` float DEFAULT '-0.116',
  `PosY` float DEFAULT '0.189',
  `PosZ` float DEFAULT '0.088',
  `RotX` float DEFAULT '0.0',
  `RotY` float DEFAULT '44.5',
  `RotZ` float DEFAULT '0.0',
  `Bone` tinyint(4) NOT NULL DEFAULT '1',
  `Hidden` tinyint(4) NOT NULL DEFAULT '0',
  UNIQUE `weapon`(`weapon`, `weaponID`)
) DEFAULT CHARSET=utf8;


*/

#include <a_samp>
#include <a_mysql>
#include <zcmd>

#define isnull(%1) ((!(%1[0])) || (((%1[0]) == '\1') && (!(%1[1]))))

#define function:%0(%1) \
			forward %0(%1); public %0(%1)

enum {
	DIALOG_BONE
}

enum weaponSetting {
	Float: wPos[6],
	wBone,
	wHidden
}
new wSettings[MAX_PLAYERS][24][weaponSetting],
	wTick[MAX_PLAYERS],
	eWeapon[MAX_PLAYERS], // variabila pentru editarea armei
	MySQL: g_SQL;

GetWeaponObjectSlot(weaponid) {
	new objectSlot;

	switch(weaponid) {
		case 2, 3, 5..9, 22..24: objectSlot = 0;
		case 25..27: objectSlot = 1;
		case 28, 29, 32: objectSlot = 2;
		case 30, 31: objectSlot = 3;
		case 33, 34: objectSlot = 4;
		case 35..38: objectSlot = 5;
	}
	return objectSlot;
}

GetWeaponModel(weaponid) {
	new wModel;

	switch(weaponid) {
		case 2: wModel = 333;
		case 3: wModel = 334;
		case 5: wModel = 336;
		case 6: wModel = 337;
		case 7: wModel = 338;
		case 8: wModel = 339;
		case 9: wModel = 341;
		case 22..29: wModel = 324 + weaponid;
		case 30: wModel = 355;
		case 31: wModel = 356;
		case 32: wModel = 372;
		case 33..38: wModel = 324 + weaponid;
	}
	return wModel;
}

PlayerHasWeapon(playerid, weaponid) {
	new pWeapon, wAmmo;

	for(new i; i < 13; i ++) {
		GetPlayerWeaponData(playerid, i, pWeapon, wAmmo);
		if(pWeapon == weaponid && wAmmo)
			return 1;
	}
	return 0;
}

WeaponIsWearable(weaponid)
	return (weaponid >= 22 && weaponid <= 38);

WeaponIsHideable(weaponid)
	return (weaponid >= 22 && weaponid <= 24 || weaponid == 28 || weaponid == 32);

function:OnWeaponsLoaded(playerid) {
	new dRows, dWeaponID, dIndex;

	cache_get_row_count(dRows);

	for(new i; i < dRows; i ++) {
		cache_get_value_name_int(i, "weaponID", dWeaponID);
		dIndex = dWeaponID - 22;

		cache_get_value_name_float(i, "PosX", wSettings[playerid][dIndex][wPos][0]);
		cache_get_value_name_float(i, "PosY", wSettings[playerid][dIndex][wPos][1]);
		cache_get_value_name_float(i, "PosZ", wSettings[playerid][dIndex][wPos][2]);
		cache_get_value_name_float(i, "RotX", wSettings[playerid][dIndex][wPos][3]);
		cache_get_value_name_float(i, "RotY", wSettings[playerid][dIndex][wPos][4]);
		cache_get_value_name_float(i, "RotZ", wSettings[playerid][dIndex][wPos][5]);		
	
		cache_get_value_name_int(i, "Bone", wSettings[playerid][dIndex][wBone]);
		cache_get_value_name_int(i, "Hidden", wSettings[playerid][dIndex][wHidden]);
	}
}

public OnPlayerUpdate(playerid) {
	if(NetStats_GetConnectedTime(playerid) - wTick[playerid] >= 250) {
		new wID, wAmmo, objectSlot, wCount, wIndex;

		for(new i = 2; i <= 7; i ++) {
			GetPlayerWeaponData(playerid, i, wID, wAmmo);
			wIndex = wID - 22;

			if(wID && wAmmo && !wSettings[playerid][wIndex][wHidden] && WeaponIsWearable(wID) && eWeapon[playerid] != wID) {
				objectSlot = GetWeaponObjectSlot(wID);

				if(GetPlayerWeapon(playerid) != wID)
					SetPlayerAttachedObject(playerid, objectSlot, GetWeaponModel(wID), wSettings[playerid][wIndex][wBone], wSettings[playerid][wIndex][wPos][0], wSettings[playerid][wIndex][wPos][1], wSettings[playerid][wIndex][wPos][2], wSettings[playerid][wIndex][wPos][3], wSettings[playerid][wIndex][wPos][4], wSettings[playerid][wIndex][wPos][5], 1.0, 1.0, 1.0);
				else if(IsPlayerAttachedObjectSlotUsed(playerid, objectSlot))
					RemovePlayerAttachedObject(playerid, objectSlot);
			}
		}

		for(new i; i <= 5; i ++) if(IsPlayerAttachedObjectSlotUsed(playerid, i)) {
			wCount = 0;

			for(new j = 22; j <= 38; j ++) if(PlayerHasWeapon(playerid, j) && GetWeaponObjectSlot(j) == i)
				wCount ++;

			if(!wCount)
				RemovePlayerAttachedObject(playerid, i);
		}
		wTick[playerid] = NetStats_GetConnectedTime(playerid);
	}
	return 1;
}

public OnPlayerConnect(playerid) {
	new connectString[70], playerName[MAX_PLAYER_NAME];

	GetPlayerName(playerid, playerName, MAX_PLAYER_NAME);

	for(new i; i < 24; i ++) {
		wSettings[playerid][i][wPos][0] = -0.116;
		wSettings[playerid][i][wPos][1] = 0.189;
		wSettings[playerid][i][wPos][2] = 0.088;
		wSettings[playerid][i][wPos][3] = 0.0;
		wSettings[playerid][i][wPos][4] = 44.5;
		wSettings[playerid][i][wPos][5] = 0.0;
		wSettings[playerid][i][wBone] = 1;
		wSettings[playerid][i][wHidden] = false;
	}
	wTick[playerid] = 0;
	eWeapon[playerid] = 0;

	mysql_format(g_SQL, connectString, sizeof(connectString), "SELECT * FROM weapons WHERE name = '%s'", playerName);
	mysql_tquery(g_SQL, connectString, "OnWeaponsLoaded", "d", playerid);
	return 1;
}

public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[]) {
	switch(dialogid) {
		case DIALOG_BONE: {
			if(response) {
				new wID = eWeapon[playerid], wName[18], pName[MAX_PLAYER_NAME], eString[150];

				GetWeaponName(wID, wName, sizeof(wName));
				GetPlayerName(playerid, pName, MAX_PLAYER_NAME);

				wSettings[playerid][wID - 22][wBone] = listitem + 1;

				format(eString, sizeof(eString), "Ai schimbat cu succes osul armei tale: %s.", wName);
				SendClientMessage(playerid, -1, eString);

				mysql_format(g_SQL, eString, sizeof(eString), "INSERT INTO weapons (name, weaponID, Bone) VALUES ('%s', %d, %d) ON DUPLICATE KEY UPDATE Bone = VALUES(Bone)", pName, wID, listitem + 1);
				mysql_tquery(g_SQL, eString);
			}
			eWeapon[playerid] = 0;
			return 1;
		}
	}
	return 0;
}

public OnPlayerEditAttachedObject(playerid, response, index, modelid, boneid, Float:fOffsetX, Float:fOffsetY, Float:fOffsetZ, Float:fRotX, Float:fRotY, Float:fRotZ) {
	new wID = eWeapon[playerid];

	if(wID) {
		if(response) {

			if(fOffsetX > 1.4)
			{
				fOffsetX = 1.4;
				SendClientMessage(playerid, -1, "Maximum X Offset exceeded, damped to maximum");
			}
		    if(fOffsetY > 1.4) {
				fOffsetY = 1.4;
				SendClientMessage(playerid, -1, "Maximum Y Offset exceeded, damped to maximum");
			}
		    if(fOffsetZ > 1.4) {
				fOffsetZ = 1.4;
				SendClientMessage(playerid, -1, "Maximum Z Offset exceeded, damped to maximum");
			}
		    if(fOffsetX < -1.4) {
				fOffsetX = -1.4;
				SendClientMessage(playerid, -1, "Maximum X Offset exceeded, damped to maximum");
			}
		    if(fOffsetY < -1.4) {
				fOffsetY = -1.4;
				SendClientMessage(playerid, -1, "Maximum Y Offset exceeded, damped to maximum");
			}
		    if(fOffsetZ < -1.4) {
				fOffsetZ = -1.4;
				SendClientMessage(playerid, -1, "Maximum Z Offset exceeded, damped to maximum");
			}
			new enum_index = wID - 22, wName[18], pName[MAX_PLAYER_NAME], objString[340];

			GetWeaponName(wID, wName, sizeof(wName));
			GetPlayerName(playerid, pName, MAX_PLAYER_NAME);

			wSettings[playerid][enum_index][wPos][0] = fOffsetX;
			wSettings[playerid][enum_index][wPos][1] = fOffsetY;
			wSettings[playerid][enum_index][wPos][2] = fOffsetZ;
			wSettings[playerid][enum_index][wPos][3] = fRotX;
			wSettings[playerid][enum_index][wPos][4] = fRotY;
			wSettings[playerid][enum_index][wPos][5] = fRotZ;

			RemovePlayerAttachedObject(playerid, GetWeaponObjectSlot(wID));
			SetPlayerAttachedObject(playerid, GetWeaponObjectSlot(wID), GetWeaponModel(wID), wSettings[playerid][enum_index][wBone], fOffsetX, fOffsetY, fOffsetZ, fRotX, fRotY, fRotZ, 1.0, 1.0, 1.0);
		
			format(objString, sizeof(objString), "Ai modificat cu succes pozitia armei tale: %s.", wName);
			SendClientMessage(playerid, -1, objString);

			mysql_format(g_SQL, objString, sizeof(objString), "INSERT INTO weapons (name, weaponID, PosX, PosY, PosZ, RotX, RotY, RotZ) VALUES ('%s', %d, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f) ON DUPLICATE KEY UPDATE PosX = VALUES(PosX), PosY = VALUES(PosY), PosZ = VALUES(PosZ), RotX = VALUES(RotX), RotY = VALUES(RotY), RotZ = VALUES(RotZ)", 
				pName, wID, fOffsetX, fOffsetY, fOffsetZ, fRotX, fRotY, fRotZ);
			mysql_tquery(g_SQL, objString);
		}
		else SendClientMessage(playerid, -1, "Ai renuntat la editarea armei.");
		eWeapon[playerid] = 0;
	}
	return 1;
}

CMD:weapon(playerid, params[]) {
	new wID = GetPlayerWeapon(playerid);

	if(!wID)
		return SendClientMessage(playerid, -1, "Nu tii nicio arma.");

	if(!WeaponIsWearable(wID))
		return SendClientMessage(playerid, -1, "Aceasta arma nu poate fi modificata.");

	if(isnull(params))
		return SendClientMessage(playerid, -1, "{FF6347}USAGE:{FFFFFF} /weapon [position / bone / hide]");

	if(!strcmp(params, "position", true)) {
		if(eWeapon[playerid])
			return SendClientMessage(playerid, -1, "Esti deja in curs de editare a altei arme.");

		if(wSettings[playerid][wID - 22][wHidden])
			return SendClientMessage(playerid, -1, "Nu poti edita pozitia unei arme care este ascunsa.");

		new wIndex = wID - 22;

		SetPlayerArmedWeapon(playerid, 0);

		SetPlayerAttachedObject(playerid, GetWeaponObjectSlot(wID), GetWeaponModel(wID), wSettings[playerid][wIndex][wBone], wSettings[playerid][wIndex][wPos][0], wSettings[playerid][wIndex][wPos][1], wSettings[playerid][wIndex][wPos][2], wSettings[playerid][wIndex][wPos][3], wSettings[playerid][wIndex][wPos][4], wSettings[playerid][wIndex][wPos][5], 1.0, 1.0, 1.0);
		EditAttachedObject(playerid, GetWeaponObjectSlot(wID));

		eWeapon[playerid] = wID;
	}
	else if(!strcmp(params, "bone", true)) {
		if(eWeapon[playerid])
			return SendClientMessage(playerid, -1, "Esti deja in curs de editare a altei arme.");

		ShowPlayerDialog(playerid, DIALOG_BONE, DIALOG_STYLE_LIST, "Weapon - Bone", "Spate\nCap\nBrat stang superior\nBrat drept superior\nMana stanga\nMana dreapta\nCoapsa stanga\nCoapsa dreapta\nPicior stang\nPicior drept\nGamba stanga\nGamba dreapta\nAntebrat stang\nAntebrat drept\nUmar stang\nUmar drept\nGat\nFalca", "Alege", "Abort");
		eWeapon[playerid] = wID;
	}
	else if(!strcmp(params, "hide", true)) {
		if(eWeapon[playerid])
			return SendClientMessage(playerid, -1, "Esti deja in curs de editare a altei arme.");

		if(!WeaponIsHideable(wID))
			return SendClientMessage(playerid, -1, "Aceasta arma nu poate fi ascunsa.");

		new wIndex = wID - 22, wName[18], pName[MAX_PLAYER_NAME], hString[150];

		GetWeaponName(wID, wName, sizeof(wName));
		GetPlayerName(playerid, pName, MAX_PLAYER_NAME);

		if(wSettings[playerid][wIndex][wHidden]) {
			format(hString, sizeof(hString), "Acum iti poti vedea %s pe corp.", wName);
			wSettings[playerid][wIndex][wHidden] = false;
		} else {
			if(IsPlayerAttachedObjectSlotUsed(playerid, GetWeaponObjectSlot(wID)))
				RemovePlayerAttachedObject(playerid, GetWeaponObjectSlot(wID));

			format(hString, sizeof(hString), "Acum nu iti mai poti vedea %s pe corp.", wName);
			wSettings[playerid][wIndex][wHidden] = true;
		}
		SendClientMessage(playerid, -1, hString);

		mysql_format(g_SQL, hString, sizeof(hString), "INSERT INTO weapons (name, weaponID, Hidden) VALUES ('%s', %d, %d) ON DUPLICATE KEY UPDATE Hidden = VALUES(Hidden)", pName, wID, wSettings[playerid][wIndex][wHidden]);
        mysql_tquery(g_SQL, hString);
	}
	else SendClientMessage(playerid, -1, "Trebuie sa specific un parametru.");
	return 1;
}